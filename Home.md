#Welcome to the Android-SDK wiki!

### Overview

This document describes the steps for technical integration process between merchant's app and PayU SDK for enabling online transactions.

### PayU Payment Gateway

PayU offers electronic payment services to merchant website through its partnerships with various banks and payment instrument companies. Through PayU, the customers would be able to make electronic payments through a variety of modes which are mentioned below:
* Credit cards
* Debit cards
* Online net banking accounts
* EMI payments
* Cash Cards

PayU also offers an online interface (known as PayU Dashboard) where the merchant has access to various features like viewing all the transaction details and can be accessed through [https://www.payu.in](https://www.payu.in) by using the username and password provided to you.

## SDK Integration

The merchant can integrate with PayU by using one of the below methods:
* Non-Seamless Integration

In Non-Seamless Integration, the Customer would move from the Merchant's Activity to PayU's payment options activity.There he would select the payment options and enter the necessary details. After this PayU would redirect the customer to the bank for further processing.

* Seamless Integration

In this mode, the merchant needs to collect the details on their own and then send them to PayU's SDK.

#### Mandatory Parameters to be posted by Merchant to PayU in Transaction Request

1. **key** : 
This parameter is the unique Merchant Key provided by PayU for your merchant account. The Merchant Key acts as the unique identifier (primary key) to identify a particular Merchant Account in our database. Example: "C0Ds8q"

2. **txnid**:
This parameter is known as Transaction ID (or Order ID). It is the order reference number generated at your (Merchant’s) end. It is an identifier which you (merchant) would use to track a particular order. It is essential that you post us a unique transaction ID for every new transaction. Example: "fd3e847h2"

3. **amount**:
This parameter should contain the payment amount of the particular transaction. Please type-cast the amount to double. Example: 10.00

4. **productinfo**:
This parameter should contain a brief product description. It should be a string describing the product (The description type is entirely your choice). Example: "tshirt100"

5. **firstname**:
Should contain the first name of the user. Example: "Abdur"

6. **email**:
Must contain the email of the customer. Example: "abcd@sample.com"

7. **surl**:
Success URL - This parameter must contain the URL on which PayU will redirect the final response if the transaction is successful. The response handling can then be done by you after redirection to this URL.

8. **furl**: 
Failure URL - This parameter must contain the URL on which PayU will redirect the final response if the transaction is failed.

9. **hash (Checksum)**:
Hash is a crucial parameter – used specifically to avoid any tampering during the transaction. For hash calculation, you need to generate a string using certain parameters and apply the sha512 algorithm on this string. Please note that you have to use pipe (|) character in between these parameters as mentioned below. The parameter order is mentioned below:
**sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)**. udf1-udf5 are User Defined Fields.We can set up to 5 User Defined Fields, which would contain has been made for you to keep any information corresponding to the transaction, which may be useful for you to keep in the database. udf1-udf5 fields are for this purpose only. It’s completely for your usage and you can post any string value in this parameter. Example:

Case 1: If all the udf parameters (udf1-udf5) are posted by the merchant. Then, **hash=sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)**

Case 2: If only some of the udf parameters are posted and others are not. For example, if udf2 and udf4 are posted and udf1, udf3, udf5 are not. Then, **hash=sha512(key|txnid|amount|productinfo|firstname|email||udf2||udf4|||||||SALT)**

Case 3: If NONE of the udf parameters (udf1-udf5) are posted. Then,
**hash=sha512(key|txnid|amount|productinfo|firstname|email|||||||||||SALT)**

### Mandatory Parameters for special cases:

1. **pg**: Defines the payment category.Please set its value to ‘NB’ for Net Banking , ‘CC’ for Credit Card , ‘DC’ for Debit Card , ‘CASH’ for Cash Card and ‘EMI’ for EMI.

2. **bankcode**: Each payment option is identified with a unique bank code at PayU. You would need to post this parameter with the corresponding payment option’s bankcode value in it. For example, for ICICI Net Banking, the value of bankcode parameter value should be ICIB. For detailed list of bank codes, please contact PayU team.

3. **ccnum**: This parameter must contain the card (credit/debit) number entered by the customer for the transaction.

4. **ccname**: This parameter must contain the name on card – as entered by the customer for the transaction.

5. **ccvv**: This parameter must contain the cvv number of the card – as entered by the customer for the transaction.

6. **ccexpmon**: This parameter must contain the card’s expiry month - as entered by the customer for the transaction. Please make sure that this is always in 2 digits. For months 1-9, this parameter must be appended with 0 – like 01,02...09.

7. **ccexpyr**: The customer must contain the card’s expiry year – as entered by the customer for the transaction. It must be of 4 digits. For example - 2017, 2029 etc. 

## Integration Procedure

*  To integrate with the PayU Android SDK, download the source code and import it into your IDE, and add it as a dependency in your project.

*  **Changes in App’s AndroidManifest.xml** : Define the following tags within your application tag- 

        <​meta­data ​android​:name=​"payu_merchant_id" ​android​:value=​"XXXXXXX"​/>
        <​meta­data ​android​:name=​"payu_merchant_salt" ​android​:value=​"XXXXXX"​/>

     3. In case of SDK Hash Generation, set SDK_HASH_GENERATION = fasle ​in Constants.java file.
     4. To disable back button on Web View, add this tag-
                
            <metadata android:name="payu_disable_back" android:value="true"/>

* **Define the following activities**:

        <activity ​android​:name=​"com.payu.sdk.PaymentOptionsActivity" ​android​:configChanges="orientation|keyboard|screenSize" ​/>
        <activity ​android​:name=​"com.payu.sdk.ProcessPaymentActivity" ​android​:configChanges=​"orientation|screenSize" ​/>

* **Add these following permissions to your app**:  

        <uses-permission android:name="android.permission.INTERNET" />
	    <uses-permission android:name="android.permission.RECEIVE_SMS" />
	    <uses-permission android:name="android.permission.READ_SMS" />

### Using SDK's UI ( Non Seamless Integration )

* In your main activity, create a HashMap (for Java ) with the mandatory parameteres. For example-

        HashMap<String, String> params = new ​HashMap<String, String>();  
        params.put(PayU.TXNID​, "0nf7" ​+ System.currentTimeMillis());  
        params.put(PayU.PRODUCT_INFO​, "product"​);  
        params.put(PayU.USER_CREDENTIALS​, "xxxxx:1msdf"​);  
        params.put(PayU.FURL​, "​https://dl.dropboxusercontent.com/s/1goxkgkx18sg5sm/failure.html​"​);  
        params.put(PayU.SURL​, "​https://dl.dropboxusercontent.com/s/dtnvwz5p4uymjvg/success.html​"​);  

* For other ( non mandatory parameters, refer Web Documentation )-

        // example enforce method
        params.put(PayU.ENFORCE_PAYMETHOD​,"creditcard|ICIB|LVRB|TMBB|EMIIC6|EMIIC12|EMIA3|YPAY|DONE|AMON"​);
        // example drop category
        params.put(PayU.DROP_CATEGORY,"CC|MAST,DC|VISA|MAST,NB|LVRB|KRKB|ADBB|TMBB,EMI|EMIIC6|EMIIC12|EMIA3,CASH|AMON"​);

* All the Constants are present in either PayU.java file or Constants.java as static variables.

* The available payment modes are -

     1. Stored card- PayU​.PaymentMode.​STORED_CARDS
     2. Credit/Debit card- PayU​.PaymentMode.​CC
     3. Net banking- PayU​.PaymentMode.​NB
     4. Emi- PayU​.PaymentMode.​EMI
     5. Cash Card- PayU​.PaymentMode.​CASH
     6. Payu Money- PayU​.PaymentMode.​PAYU_MONEY

* What if I want to use only specific Payment Modes, such as CC, NB or Cash? ( However sdk makes background verification on given payment modes for the merchant key. )

        PayU​.getInstance(MainActivity.​this​).startPaymentProcess(​100​, params, ​new ​PayU​.PaymentMode[] {​PayU.PaymentMode.​CC​, ​PayU​.PaymentMode.​NB​, ​PayU​.PaymentMode.​CASH​});

* What if I want to change the order of payment modes to be displayed ?

        PayU.getInstance(MainActivity.this).startPaymentProcess(100,params,new PayU.PaymentMode[{PayU.PaymentMode.PAYU_MONEY, PayU.PaymentMode.STORED_CARDS, PayU.PaymentMode.CC});

### Using Your Own UI ( Seamless Integration, RECOMMENDED )

* Using your own UI, apart from the main Payment Process, you can also make use of SDK's various public functions, such as:
    1. Card Validations.
    2. Netbanking Downtime notification.
    3. Issuing bank downtime notification.
    4. Payu’s Process payment Activtity (Which takes care of payu’s payment process and it includes CustomBrowser).
    5. PayU’s Webservices using GetResponseTask­ It is an asynchronous task with android’s httpclient.

* What are all the web services can i make use of ?

        public static final ​String GET_USER_CARDS ​= "get_user_cards"​;
        public static final ​String DELETE_USER_CARD ​= "delete_user_card"​;
        public static final ​String SAVE_USER_CARD ​= "save_user_card"​;
        public static final ​String GET_VAS ​= "vas_for_mobile_sdk"​;
        public static final ​String PAYMENT_RELATED_DETAILS ​= "payment_related_details_for_mobile_sdk"​;

* How to make payment after collecting Credit Card details from my own UI?

Make sure you pass all the parameters- the Mandatory ones as well as the ones required for Seamless Integration. Example-

		public void makePayment()  {
		Payment payment;
		Params​requiredParams = ​new ​Params​();
		Payment​.​Builder builder ​=​​new​Payment()​.new​Builder();
		builder.set(PayU.​PRODUCT_INFO​, ​"product"​);
		builder.set(PayU.​AMOUNT​, ​"5.0"​);
		builder.set(PayU.​TXNID​, String.​valueOf​(​"Production :" ​+ System.​currentTimeMillis​()));
		builder.set(PayU.​SURL​, ​"​https://dl.dropboxusercontent.com/s/dtnvwz5p4uymjvg/success.html​"​);
		builder.set(PayU.​MODE​, String.​valueOf​(PayU.PaymentMode.​CC​));
		requiredParams​.put(PayU.​CARD_NUMBER​, ​”5123456789012346”​);
		requiredParams​.put(PayU.​EXPIRY_MONTH​, ​“12”​);
		requiredParams​.put(PayU.​EXPIRY_YEAR​, ​“2020”​);
		requiredParams​.put(PayU.​CARDHOLDER_NAME​, ​”My Test Card”​);
		requiredParams​.put(PayU.​CVV​, ​“123”​);
		requiredParams​.put(PayU.​AMOUNT​, builder.get(PayU.​AMOUNT​));
		requiredParams​.put(PayU.​PRODUCT_INFO​, builder.get(PayU.​PRODUCT_INFO​));
		requiredParams​.put(PayU.​TXNID​, builder.get(PayU.​TXNID​));
		requiredParams​.put(PayU.​SURL​, builder.get(PayU.​SURL​));
		requiredParams​.​put(PayU​.​MERCHANT_KEY​, “​XXXXXX”​);
		if ​(getArguments().getString(PayU.​STORE_CARD​) != ​null​) { 
			​// to Store a card .
			​requiredParams.put(​"card_name"​, ​cardName​);
			requiredParams.put(PayU.​STORE_CARD​, ​"1"​);
		}
		payment ​=​builder​.​create();
	String postData = PayU.​getInstance​(getActivity()).createPayment(payment,requiredParams);
	Intent intent = ​new ​Intent(getActivity(), ProcessPaymentActivity.​class​);
	intent.putExtra(Constants.​POST_DATA​, postData);
	intent.addFlags(Intent.​FLAG_ACTIVITY_CLEAR_TOP​);
	getActivity().startActivityForResult(intent, PayU.​RESULT​);

	}
At this point you should be able to see the 3d-secure page.
The postData should have some thing like this.

instrument_type=Manufacturer%3A+unknown+Model%3A+Android+SDK+built+for+x86++Product%3A+sdk_x86&txnid=1426079940083&ccexpyr=2018&ccnum=5123456789012346&device_type=1&productinfo=myprod&card_name=helod&amount=10.0&pg=CC&user_credentials=ra%3Ara&bankcode=CC&ccvv=988&surl=https%253A%252F%252Fdl.dropboxusercontent.com%252Fs%252Fz69y7fupciqzr7x%252FfurlWithParams.html&instrument_id=81aac0aa7fd6573&store_card=1&ccname=helo&ccexpmon=3&key=xxxxxx&hash=845af2e84cb831efa5e6edc2111d4a15b13c42bbaa20e58a2401d71406289ce8d5d7cfc2d3016eef103fec939a7a811aff5ee2fa9f87b763dc7ebc6c05a1d63a

Please refer [this](https://github.com/payu-intrepos/Android-SDK-Sample-App/blob/v2/feature/creditCardExample/app/src/main/java/com/payu/sample/MainActivity.java) for live example

* What about netbanking with My UI ?

Doing this is the same as Credit Card/Debit Card the only change is payment mode should be set to NB. 

    builder.set(PayU.MODE, String.valueOf(PayU.PaymentMode.NB));
    requiredParams.put(PayU.BANKCODE, bankCode);

Please refer [this](https://github.com/payu-intrepos/Android-SDK-Sample-App/blob/v2/feature/netbankingExample/app/src/main/java/com/payu/sample/MainActivity.java) for live card example. Note: Cash Card follows Netbanking, EMI follows NB and CC 

* What about card validations?

Cards validation is done by various functions present in Cards.java file-

 1. For length+luhn validation. : `Cards.​validateCardNumber​("5123456789012346")`
 2. For only luhn validation : `Cards.​luhn​(​“5123456789012346”​)`
 3. For CVV validation : `Cards.​validateCvv​(​“5123456789012346”​, ​“123”​)` 
 4. To get the issuer of the card : `Cards.​getIssuer​(​“5123456789012346”​)` 

* Do you have any issuer icons which I can show on my card number edit text view ?

Yes we have.

	Cards​.​initializeIssuers​(getResources());
	String issuer​= Cards.​getIssuer​(​cardNumber​);
	Drawable​issuerDrawable ​= Cards.​ISSUER_DRAWABLE​.get(​issuer​);
	((EditText)getActivity().findViewById(R.id.​cardNumberEditText​)).setCompoundDrawablesWithIntrinsicBounds(​null​, ​null​, ​issuerDrawable​, ​null​);

* How to enable the VAS ?

    1. implement PaymentListener your Activity/Fragment
    2. add the following code in your onCreateView.

`public class ​CardsFragment extends ​Fragment implements ​PaymentListener{`

	@Override

	public ​View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		if​(Constants.ENABLE_VAS ​&& PayU.issuingBankDownBin == null​){ // vas has not been called, lets fetch bank downtime.
		HashMap<String, String> varList = new ​HashMap<String, String>();
		varList.put("var1"​, "default"​);
		varList.put("var2"​, "default"​);
		varList.put("var3"​, "default"​);
		List<NameValuePair> postParams = null​;

				try ​{
				postParams = PayU.getInstance(getActivity()).getParams(Constants.GET_VAS​, varList);
				GetResponseTask getResponse = new ​GetResponseTask(CardsFragment.this​);
				getResponse.execute(postParams);
				} 
				catch ​(NoSuchAlgorithmException e) {
				e.printStackTrace();
				}
		}
	}
			@Override
			public void ​onPaymentOptionSelected(PayU.PaymentMode paymentMode) {
			}
			@Override
			public void ​onGetResponse(String responseMessage) {
			// make sure that you get success response message here.
			// PayU.issuingBankDownBin will have issuing bank down details.
			// PayU.netBankingStatus will have net banking status
			}
	}

Now you can find a bank’s down status as follows:


	if​(PayU.issuingBankDownBin != null ​&& PayU.issuingBankDownBin.has(cardNumber​.substring(0, 6)))

	{ // oops bankis down.
	  // set custom down notification.

	}

	else​{

	// nope, good to go

	}

Now you can find netbanking down status as follows:


	if​(PayU.netBankingStatus != null ​&& PayU.netBankingStatus.get(bankCode​) == 0){

	// opps this bank is down

	}else​{

	// nope, good to go

	}

* **For Server Hash Generation** : 

 In the Constants.java file in PayU SDK, set SDK_HASH_GENERATION=false. Also set corresponding hash values in Payu.java -

        public static ​String paymentHash;
        public static ​String deleteCardHash;
        public static ​String getUserCardHash;
        public static ​String editUserCardHash;
        public static ​String saveUserCardHash;
        public static ​String merchantCodesHash;
        public static ​String vasHash;
        public static ​String ibiboCodeHash;

Every api call requires hash, sdk will automatically take corresponding hash according to the ​command.

For example, JAVA code to calculate paymentHash is given below:

	
		try ​{
		MessageDigest messageDigest = MessageDigest.getInstance("SHA­512"​);
		String postData = mMerchantKey + "|" ​+ params.get("txnid"​) + "|" ​+ params.get("amount"​) + "|" ​+
		params.get("productinfo"​) + "|" ​+ params.get("firstname"​) + "|" ​+ params.get("email"​) + "|"​;
			for ​(int ​i = 1; i <= 10; i++) {
			postData += params.get("udf" ​+ String.valueOf(i)) + "|"​;
			}

		postData += mSalt;
		messageDigest.update(postData.getBytes());
		byte​[] mdbytes = messageDigest.digest();
		StringBuffer hexString = new ​StringBuffer();
			
			for ​(byte ​hashByte : mdbytes) {
			// hexString.append(Integer.toHexString(0xFF & hashByte));
			hexString.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
			}
		return ​hexString.toString();

		} 
		catch ​(NoSuchAlgorithmException e) {
		throw new ​HashException();
		} 
		catch ​(UnsupportedEncodingException e) {
		e.printStackTrace();
		}

For better understanding of hash calculation please refer our web integration document. 


## Web Services- APIs

**PayU has made many web-services for you. Each web-service has a specific function and hence can
be used to automate different features. The basic format and execution of all web-services remains
the same. Each web-service is a server call from your app to PayU’s server.**

### Url's to be used:

 **For Production Server :**

* https://info.payu.in/merchant/postservice.php?form=1

(form=1 shall return output in array form)

* https://info.payu.in/merchant/postservice.php?form=2

(form=2 shall return output in json form)

**For Test Server :**

* https://test.payu.in/merchant/postservice.php?form=1

(form=1 shall returns output in array form)

* https://test.payu.in/merchant/postservice.php?form=2

(form=2 shall return output in json form)

**Some useful APIs for Android:**

* **get_user_cards** This API is used to fetch all the cards corresponding to the user. In this API, card number and other sensitive information is not returned.

Input Parameters: **var1** user_credentials (In the format- MerchantKey:UserIdentifier)

Response: 

When cards are found: 


		array
		(
		[status] => 1
		[msg] => Cards fetched Succesfully
		[user_cards] => Array
			(	
			[745d72e2fd9b7e88824fef4e7ed7dac1fe624b74] => Array
				(
				[card_name] => nickname but if sent empty then (cardType****last 4 digits of card) e.g. mastercard****2346
				[name_on_card] => {name}
				[card_type] => CC(ibibo_code)
				[card_token] => 745d72e2fd9b7e88824fef4e7ed7dac1fe624b74
				[is_expired] => 1(1 when card is expired , 0 when not)
				[card_mode] => CC(card Category)
				[card_no] => 412345xxxxxx2356(masked Card Number)
				[card_brand] => VISA
				[card_bin] => 412345
				[expiry_year] => 2017
				[expiry_month] => 10
				)
			)
		)

When no cards are found:


		array
		(
		'status' => 0, 
		'msg' => Card not found
		)

* **delete_user_card**	This API is used to delete a card. 

Input parameters: **var1** user_credentials - merchantKey:userId , **var2**- cardToken (cardtoken of the card to delete)

Response that we are getting:

**On Successful deletion of card:** 


		array
		(
		'status' => 1, 
		'msg' => {cardName} deleted successfully'
		)

**On failure of deletion:** 


		array
		(
		'status' => 0, 
		'msg' => error reason
		)	


For a comprehensive list of all APIs please refer to the [Web Integration Document](https://drive.google.com/a/payu.in/file/d/0B4URmsDLhGXmOWczQ3A5dnBOUzNEV2lwSVloaDFybnNUQUR3/edit)